# Proceso de atacado de PCB con Cloruro Férrico

## Antecedentes

La manufactura de tarjetas de circuito impreso permite hacer prototipos y probar circuitos de forma rápida económica y segura.

Principalmente existen dos procesos para realizar PCBs uno consiste en el trazo de las pistas por medio de una broca. Esto se logra con una máquina de Control Numérico Computacional (CNC).

La otra consiste en corroer el cobre con alguna sustancia que reaccione con él. Para ello es necesario que las pistas estén protegidas ya sea con toner, tinta UV o incluso con tinta de un plumón permanente.

En este caso ya se realizo la termotransferencia de las pistas que estarán protegidas en el PCB y el atacado se realizará con Cloruro Férrico.

## Precauciones
EL cloruro férrico es altamente corrosivo y no conviene estar en contacto con el. Por ello se sugiere utilizar el siguiente Equipo de Protección Personal (EPP):

* Guantes de latex
* Lentes protectores
* Bata
* Mascarilla

En caso de entrar en contacto con el cloruro férrico es recomendable lavarse con abundante agua y jabón ya que el cloruro férrico puede neutralizarse con sosa.

Es importante evitar inhalar los gases que se desprenden cuando el cloruro férrico y el cobre están en contacto.

## Material

* Placa fenólica con el circuito que se va a elaborar
* Cloruro férrico
* Agua oxigenada

## Equipo
Es importante contar con lo siguiente

* Papel o franela para limpiar posibles escurrimientos
* Recipiente para colocar la placa durante el proceso de atacado (debe ser plastico o de vidrio)
* Recipiente para guardar el cloruro activo (debe ser plastico o de vidrio)
* Recipiente para guardar el cloruro usado (debe ser plastico o de vidrio)

El último recipiente se utiliza cuando el cloruro activo que ya no reacciona con el cobre o tarda mucho tiempo en reaccionar.

**Nota**: No debe arrojarse el cloruro usado en el drenaje.

## Preparación del Cloruro férrico

La presentación del cloruro férrico que utilizamos es líquida, esta debe activarse para que el atacado del cobre se efectivo.

La proporción es la siguiente:

* Dos porciones de cloruro Férrico
* Una porción de Agua

Existen diversas maneras para que el tiempo de atacado sea menor, las mas sencillas son:

* Calentar la solución (Se puede calentar previamente la porción de agua)
* Oxigenar la solución (Se puede utilizar agua oxigenada)

## Proceso

Una vez preparado el cloruro férrico se procede de la siguiente forma:

1. Colocar la placa en el recipiente.
1. Vertir el cloruro férrico preparado en el recipiente. (en este momento debe observarse un burbjeo y comienzan a desprenderse gases)
1. Mover levemente el envace para que se generen pequeñas olas. Esto acelera el proceso de atacado.
1. Después de unos minutos el cobre que no está protegido se desprende totalmente de la placa y solo quedan las pistas de interés.
1. Al terminar sacamos la placa con ayuda de un palito de madera y lo limpiamos con papel o franela el exceso de cloruro
1. Se lava la placa con una mezcla abundante de jabón y agua.
1. El cloruro restante se guarda en el recipiente etiquetado de como cloruro preparado.
1. Se limpia el exceso de cloruro férrico con papel o franela y se lava el recipiente una mezcla abundante de jabón y agua.

![Imagen atacado con cloruro férrico](img/atacadoCloruro.jpg "Imagen atacado con cloruro férrico")

![Imagen limpieza de exeso de cloruro férrico](img/limpiezaExcesoCloruro.png "Imagen atacado con cloruro férrico")

![Imagen de placa lavada](img/placaLavada.png "Imagen atacado con cloruro férrico")

![Imagen de recoleccion de cloruro](img/recoleccionDeCloruro.png "Imagen de recolección del cloruro férrico")

## Procesos finales

Ahora se generan los barrenos (orificios) de los componentes Through-hole technology (THT) y para los orificios de montaje.

Esto puede hacerse con un taladro de banco o con un taladro manual. Existen muchos proyectos sobre taladros realizados con moteres de carros de juguete.

![Imagen barreno de placa](img/barrenos.jpg "Imagen de realización de barrenos en la placa ")

## Capas sugeridas

Si bien hasta este momento es posible soldar losc  mponentes u probar el PCB, tambien es importante considerar las siguientes capas en caso de tener posibilidad de hacerlas:

* **Capa de Sorder Mask:** Esta capa protege de la oxidacion el cobre y facilita el proceso de soldadura.

* **Capa de serigrafía:** Esta capa permite identificar la ubicacion de los componentes su etiqueta y en muchos casos también su valor.

![Imagen placa de prototipo finalizada](img/placaFinalizada.jpg "Imagen de placa finalizada")
