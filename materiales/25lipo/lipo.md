% Baterías de Litio
  *Introducción*
% [gitlab.com/dpw19/electroemprende](gitlab.com/dpw19/electroemprende)
% 02/12/2020

# Baterías de litio *Introducción*
Pavel E. Vázquez Mtz.

*electroemprende19@riseup.net*

---

## Litio
El litio (Li) es un material que se utiliza ampliamente en la actualidad entre otras cosas para la elaboración de baterías, principalmente de Iones de Litio y de Polímeros de litio LiPo.

---

## ¿Petróleo blanco? Litio

El enlace muestra un video de TV UNAM titulado  **¿Petróleo blanco? Litio**

[Video](https://hooktube.com/watch?v=Bw5TVZrFYus)

---

## Nuevas tecnologías

En la actualidad existen también nuevas tecnologías de baterías basadas en el litio [1], [2] y constantemente se está innovando e este terreno por la alta demanda de almacenamiento que se tienen con los vehículos eléctricos [3].

---

## Li-Po vs Li-Ion

Las baterías Li-Po están empaquetadas en un aluminio y plástico y cuentan con dispositivos que las protejen de sobre-corrientes y de sobre-voltajes. Al ser construidas con polímeros de litio estas permiten formas diversas. [4]

---

Las baterías Li-Ion tienen un empaquetado cilíndrico, que contiene óxido de cobaldto de litio. El empaquetado común de estas baterías es al 18650 y no cuenta con circuitos de protección internos por lo que estos deben agregarse de forma externa. [4]

---

La figura muestra la apariencia de las baterias Li-Po y Li-Ion.

 ![LiPo vs LiIon](https://www.easyacc.com/media-center/wp-content/uploads/2016/02/18650-and-li-Polymer-Battery.jpg "Imagen de https://www.easyacc.com/media-center/what-is-the-difference-between-18650-and-li-polymer-battery/")

---

## Volatilidad

El litio reacciona violentamente bajo muchas condiciones, por ello es importante almacenarlas adecuadamente.

---

En el video se observa qué le sucede a una batería LiPo cuando esta se encuentra en condiciones extremas. También muestra lo que sucede con las bolsas de almacenamiento.

[Video](https://hooktube.com/watch?v=CnNId0mDnBo)

---

Este video muestra la reaccion de las baterías LiPo cuando estas son perforadas intencionalmente

[Video](https://hooktube.com/watch?v=ZiRR3GbbR8k)

---

## Carga de las baterías de Litio

Tanto las celdas Li-Po como las Li-Ion tienen un voltaje nominal de 3.7[V], lo que significa que se encuentran descargada y con vida útil.

Después de someterse aun proceso de carga del Corriente Constante - Voltaje Constante (CC-CV) la celda debe tener 4.2[V]. [5]

---

## Ciclo de carga CC-CV
El ciclo de carga Corriente Constante - Voltaje Constante es de la sigiente forma:

1. Se inyecta una corriente constante de un valor determinado mientras el voltaje va aumentando.

2. Cuando el voltaje tiene un valor de 4.2 [V] entonces el nivel de voltaje se mantiene constante y la corriente comienza a decaer hasta llegar a un valor muy bajo, casi cero.

3. En este momento se puede decir que la celda tiene su máxima capacidad.

---

# Gráfica de carga

En las hojas de datos puede encontrarse la siguiente gráfica de carga [6], [7].

![Grafica de carga de celdas Li-po, Li-Ion](https://blog.bricogeek.com/img_cms/3137-03.jpg "Imagen de https://blog.bricogeek.com/noticias/tecnologia/todo-lo-que-necesitas-saber-sobre-la-baterias-de-ion-litio-lipo/")

---

# Referencias
[1] [https://www.hibridosyelectricos.com/articulo/tecnologia/asi-es-bateria-estructural-tesla-modulos-celdas-4680/20201127160105040406.html](https://www.hibridosyelectricos.com/articulo/tecnologia/asi-es-bateria-estructural-tesla-modulos-celdas-4680/20201127160105040406.html)

[2] [https://www.hibridosyelectricos.com/articulo/tecnologia/plataforma-egmp-coches-electricos-hyundai-datos/20201202095809040509.html](https://www.hibridosyelectricos.com/articulo/tecnologia/plataforma-egmp-coches-electricos-hyundai-datos/20201202095809040509.html)

[3] [https://www.hibridosyelectricos.com/articulo/tecnologia/5-tecnologias-produccion-baterias-litio-cobalto-nivel-industrial/20200207100955033008.html](https://www.hibridosyelectricos.com/articulo/tecnologia/5-tecnologias-produccion-baterias-litio-cobalto-nivel-industrial/20200207100955033008.html)

[4] [https://www.easyacc.com/media-center/what-is-the-difference-between-18650-and-li-polymer-battery/](https://www.easyacc.com/media-center/what-is-the-difference-between-18650-and-li-polymer-battery/)

---

[5] [http://www.baterialipo.es/2016/05/20/cargando-baterias-lipo/](http://www.baterialipo.es/2016/05/20/cargando-baterias-lipo/)

[6] [https://www.ineltro.ch/media/downloads/SAAItem/45/45958/36e3e7f3-2049-4adb-a2a7-79c654d92915.pdf](https://www.ineltro.ch/media/downloads/SAAItem/45/45958/36e3e7f3-2049-4adb-a2a7-79c654d92915.pdf)

[7] [https://www.jameco.com/Jameco/Products/ProdDS/2144243.pdf](https://www.jameco.com/Jameco/Products/ProdDS/2144243.pdf)

[8] [https://blog.bricogeek.com/noticias/tecnologia/todo-lo-que-necesitas-saber-sobre-la-baterias-de-ion-litio-lipo/](https://blog.bricogeek.com/noticias/tecnologia/todo-lo-que-necesitas-saber-sobre-la-baterias-de-ion-litio-lipo/)
