# Taller de Electrónica ¡ElectroEmprende!

Te invitamos a conocer los objetivos y detalles que debes conocer para participar 
en este taller.

# Objetivo

Las personas participantes desarrollarán, implementarán y darán seguimiento a un
proyecto que genere productos o servicios mediante el estudio y aplicación de la 
electrónica, fomentando así la autonomía económica.

# Perfil

Si bien el taller está abierto a todas las personas interesadas será más fácil para 
ti si tienes el siguiente perfil:

* Gusto por el trabajo colaborativo
* Disposición
* Iniciativa e inventiva
* Deseo de adentrarse al mundo de la electrónica
* Deseo/gusto de emprender


# Flujo de Trabajo

Para alcanzar los objetivos del taller proponemos trabajar de forma paralela en un
proyecto de emprendimiento de productos o servcios, a la vez que se avanza en los
conocimientos de la electrónica.

De esta forma se pueden aplicar los conocimientos 
generados en forma práctica.

Es importante que pienses en algo que desees realizar y que tenga relación con la 
electrónica, en este momento no importa que aún no tengas conocimientos previos en 
la materia, ya que los módulos consideran una sección teórica y contarás con 
acompañamiento para aterrizar los conocimientos aplicados a tu proyecto.

# Permanencia en el taller

Para considerar tu permanencia en el taller es importante que participes activamnte en los siguientes aspectos:

* Pensar en al menos un producto o un servicio para su emprendimiento.
* Realizar las actividades propuestas en la plataforma moodle.
* Dar seguimiento a los avances de tus propuestas
* Realizar y presentar avances, prototipos que se aproximen sucesivamente a una 
conclusion del proyecto.
* Retroalimentar y dar seguimiento a otros proyetos

**¡El trabajo colaborativo beneficia a todas las personas!**

# Medios de contacto

Tanto para inscribirte al taller de Electrónica: ¡Electroemprende!, como al 
grupo de Telegram escribe un correo a

    actividadespantitlanpls@gmail.com

# Seguimiento a personas inscritas

Los **avances de cada proyecto** se publican en el siguiente 
[enlace](emprendimientos/) 

La **comunicación directa** se hace por el grupo de [Telegram](telegram.org)
(para obtener el enlace del grupo debes solicitarlo al correo 
actividadespantitlanpls@gmail.com)

Las **videocharlas** se realizan via meet.jit.si
(El enlace se comparte en el grupo de Telegram)

Las **actividades en línea** se realizan en el siguiente 
[enlace](https://capacitacion.cdmx.gob.mx)
(Para obtener tus credenciales de acceso debes solicitarlas al correo 
actividadespantitlanpls@gmail.com)


# Horarios

**Lunes y miércoles:** Videocharlas (clases) de 18 a 20 horas  
**Martes y jueves:** Videocharlas (proyectos) de 18 a 20 horas  
**Lunes a viernes:** De forma continua en Telegram  
**Lunes a viernes:** Debes considerar al menos 4 horas por semana para tus 
actividades en la plataforma
