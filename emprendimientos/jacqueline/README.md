# Proyecto de emprendimiento

Realizado por Jacqueline Gómez Pastrana
Lámpara de dibujos o Caja de luz

# Descripcion del proyecto

El proyecto es la creación de lámparas decorativas de escritorio, hechas con tiras de leds y dibujos.
Los dibujos quedan colgando dentro de una caja de cristal (o transparente) sujetos con hilos transparentes en la parte de arriba, dando el efecto de que estén flotando.

La el dibujo que contengan será hecho a mano o impreso en papel acetato o papel albanene, los dibujos pueden ser obras de arte reconocidas o dibujos hechos por cualquier artista.


# Aportaciones al proyecto (Fernando)

## Qué tan viable es el proyecto

## Que dificultades le notas

## Qué sugieres para que sea más viable

* Buena idea!!!

* Si el propósito es una lampara de escritorio, podría ser giratoria la pantalla y agregar cambio de color con leds RGB.

* Se puede agregar un lente para tener el acetato tamaño pequeño.
* Si se agregan magnetos se puede hacer flotantes aunque sean unos pocos centímetros.

* Si se maneja cómo pilinora podría ponerse un mini arduino y alimentación con 
baterías para hacer que se activen los cambios de luz con cambio de intensidad.

* Definir la pantalla para ver como se iluminará.

* Definir la alimentación (pila o eliminador de baterías)


# Aportaciones al proyecto (Haziel)

## Qué tan viable es el proyecto

## Que dificultades le notas

## Qué sugieres para que sea más viable

# Presentación del proyecto

![Diapositiva 01](imagen/photo_2020-07-23_17-22-38.jpg)

![Diapositiva 01](imagen/photo_2020-07-23_17-22-42.jpg)

![Diapositiva 01](imagen/photo_2020-07-23_17-22-45.jpg)

![Diapositiva 01](imagen/photo_2020-07-23_17-22-47.jpg)

![Diapositiva 01](imagen/photo_2020-07-23_17-22-49.jpg)

![Diapositiva 01](imagen/photo_2020-07-23_17-22-52.jpg)

![Diapositiva 01](imagen/photo_2020-07-23_17-22-54.jpg)

# Pasos a seguir

