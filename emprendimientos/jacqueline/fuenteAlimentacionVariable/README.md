# Fuente de alimentación

Este es un proyecto de emprendimiento realizado en el taller de Electrónica ¡Electroemprende! en el Pilares Agrícola Pantitlán.

Se trata de una fuente de alimentación regulada con voltaje variable montada en un gabinete con la finalidad de tener un dispositivo versátil que permita brindar energía a los demás proyectos que se desarrollen a lo largo del taller.

Colaboraron en este proyecto:

* Jacqueline Anahí Gómez Pastrana, jacquestrella@hotmail.com
* Pavel E. Vazquez Mtz. pavel_e@riseup.net

Todos los esquemáticos son Hardware Libre y están en constante actualización, puedes obtener las versiones más recientes en el
[repositorio](https://gitlab.com/dpw19/fuentes).

# Introducción

En el contexto de la electrónica siempre se cuenta con una fuente para alimentar los circuitos de los dispositivos que usamos cotidianamente ya sea un dispositivo móvil, una pantalla o una pc, etc.

La energía que consumimos proviene de diversas fuentes y se genera en centrales termoeléctricas, hidroeléctricas, solares, eólicas, etc.

Para que la distribución de la energía eléctrica sea más eficiente se opta por la corriente Alterna(CA), de esta forma se mitiga el efecto de la resistencia y se disminuye el calentamiento en los conductores. La energía eléctrica que llega a los hogares de México es de tipo senoidal con un valor nominal RMS de 127[V] a 60[Hz].

La gran mayoría de los equipos electrónicos domésticos funcionan con voltajes de
corriente directa entre 3.3[V] y 24[V], para convertir los voltajes de Corriente Alterna a voltajes de corriente directa se utiliza una fuente de alimentación. En la actualidad existen dos tipos de fuentes de alimentación las fuentes reguladas y las fuentes conmutadas.

En este caso se abordará un diseño simple de una fuente regulada basada en el circuito regulador variable LM317 y el circuito regulador fijo LM7805.

# Descripción del circuito.

Se trata de una fuente de voltaje regulada con voltímetro integrado, basada en los circuitos integrados LM317 y LM7805

Las etapas de este circuito se describen en el diagrama de bloque a continuación.

![Diagrama a bloques](img/photo_2020-10-22_11-51-36.jpg "Diagrama de boloques de una fuente regulada de voltaje variable, cada bloque se describe en las siguientes secciones")


## Transformador
Se utiliza un transformador de 127[V] a 24[V] con derivación central a 1 [A].

La función de este componente es reducir el voltaje de la línea eléctrica a 24 volts RMS aproximadamente.

### Puente de diodos o puente rectificador

El puente de diodos o puente rectificador es un arreglo de cuatro diodos cuya función es obtener el valor absoluto del voltaje que tiene en la entrada. De esta forma se obtiene una señal pulsante. A esta etapa también se le conoce como rectificador de onda completa.

### Filtro

Esta etapa funciona como un filtro que permite suavizar la señal pulsante para
obtener una señal rectificada con un pequeño riso.

El filtrado se  obtiene por medio de uno o varios capacitores de valores
grandes, alrededor de 1000uF.

Se puede robustecer esta etapa agregando varios filtros RC en serie.

En la siguiente imagen se puede observar esta etapa en Tablilla de
Circuito Impreso (PCB, por sus siglas en inglés).

![Circuito impreso puente y filtro](img/photo_2020-10-22_11-50-11.jpg "Tablilla de circuito impreso del puente rectificador y del filtro con capacitor.")

### Regulador LM317

El regulador LM317 tiene 3 pines de conexión: Ajuste, entrada y salida. Este circuito regulador está diseñado de modo que mantiene un voltaje constante de 1.2 [v] entre los pines de ajuste y salida.

El regulador LM317 tiene distintas configuraciones según la aplicación en la  
que se vaya a utilizar, en este caso será un regulador variable que va de 1.2
[V] a 27 [V] aproximadamente.

El voltaje de salida de la fuente regulable es controlado por una resistencia variable de 5K.

### Regulador LM7805

El regulador LM7805 genera un voltaje de 5V en su pin de salida siempre y cuando el voltaje en su pin de entrada un poco mayor.

El voltaje de salida de los reguladores de la serie LMxx se puede identificar por los dos últimos dígitos de su matrícula.

Al igual que la mayoría de los reguladores, cuenta con 3 pines:

* **Entrada:** Pin de entrada. Acepta un voltaje de 7 [V] a 36[V] de corriente
continua, generalmente con un pequeño riso.
* **Tierra:** Establece la tierra para el regulador.
* **Salida:** Es el voltaje regulado de 5[V] de corriente continua.

En este caso el regulador 7805 es utilizado para conectar la pantalla del voltímetro-Amperímetro digital, debido a que su voltaje de alimentación va de los 4.5 [V] a los 30 [V] de corriente continua.

# Circuito esquemático.
Para la realización de esta fuente, se hicieron 3 circuitos esquemáticos con ayuda del sofware libre KiCad, dichos circuitos se muestran a continuación.

## Puente de diodos.
Como puede observarse, el circuito esquemático, esta constituido por dos tarjetas.

* **Tarjeta 1:** En esta tarjeta se recibe un voltaje de entrada de 127 VAC, ademas tiene un interruptor que permitirá o frenará el flujo de la corriente,
un fusible que protege a los elementos de la fuente en caso de haber un corto circuito o sobrecorriente.

* **Tarjeta 2:** Contiene el puente de diodos que hace la rectificación de onda completa de la señal de entrada de AC. Posteriormente entra el filtro de condensadores generando un rizado y suavizando la señal pulsante.

![Esquemático puente y filtro](img/photo_2020-10-22_11-51-33.jpg "Diagama esquemático donde se observa la etapa de rectificacion y filtrado")

## Regulador de voltaje LM317.
El circuito esquemático muestra más a detalle los elementos conectados al regulador de voltaje LM317, se muestran los las conexiones de cada pin del regulador. Como puede observarse, se tiene un voltaje de entrada que puede ir desde los 1.2[V] a los 37[V], la resistencia variable RV1 de 5K representa el potenciómetro que irá conectado al pin de ajuste del regulador LM317.  

![Esquemático LM317](img/photo_2020-10-22_11-51-34.jpg "Diagama esquemático para el regulador de voltaje 7805")

## Regulador de voltaje LM7805

![Esquemático 7805](img/photo_2020-10-22_11-51-32.jpg "Diagama esquemático para el regulador de voltaje 7805")

# BOM (lista de componentes)
* Regulador de voltaje LM317.
* 1 resistor de 220 [Ohms].
* 1 potenciómetro de 5K [Ohms].
* 1 capacitor electrolítico de 220u[f]F a 50[V].
* Transformador de 127 [V] a 24 [V], 500m [A] o 1000m [A].
* 4 diodos 1N4007.

# Circuito impreso

El proyecto completo consta de tres tarjetas de circuito impreso, se muestra su version en 3D, los archivos de diseño en KiCAD, archivos para producción en PDF y Gerber pueden descargarse del [repositorio](https://gitlab.com/dpw19/fuentes).

## Tarjeta de transformador y puente de diodos

La siguiente imagen muestra la tarjeta donde se conecta la toma de 127VAC, el interruptor, el fusible y el primario del transformador. Tmambien se muestra la tarjeta del rectificador de onda completa y el filtro.

![Tarjeta 1 y tarjeta 2](img/puenteDiodos.png "tarjeta donde se conecta la toma de 127VAC, el interruptor, el fusible y el primario del transformador. Tmambien se muestra la tarjeta del rectificador de onda completa y el filtro")



# Montaje

## Materiales para montaje de la fuente:

* Caja de plastico para meter los circuitos. (Puede ser un Tupper)
* Bornes para chasis rojo y negro.
* Un portafusible con fusible americano o europeo para chasis (500mA o 1A, según el transformador).
* Interruptor para chasis de un polo dos tiros.
* Pantalla Voltímetro y amperímetro dígital.
* Tornillos de 1/8 x 1" con tuerca y rondana.
* Cable rojo calibre 22AWG.
* Cable negro calibre 22AWG.

# Herramientas

* Taladro de mano
* Broca de 1/8 para madera
* Broca de 3/4 para madera
* Broca de 1/2 para madera
* Pinzas de corte
* Pinzas de Punta
* Pinzas pelacable
* Desarmador de cruz
* Empaques para baño
* Desarmador plano

##Proceso del montaje de la fuente.

Es importante que al momento de instalar la fuente se hagan las perforaciones y se consideren las medidas correctas, por lo que es recomendable que se marque el espacio dónde se pondrá lo siguiente:

* Orificio del cable para conectar el transformador a una fuente de 127[V] AC  
* Orificio para el interruptor
* Orificio para fusible
* Orificio para potenciómetro.
* Orificio para pantalla de multímetro.
* Orificios para bornes de chasis.
* Orificios para fijar las placas de los circuitos

La siguiente imagen muestra el montaje de todos los compionentes en el gabinete.

![Montaje fuente](img/photo_2020-10-22_11-50-41.jpg "Montaje de la Fuente de alimentación")

En la siguiente imagen se tiene una vista frontal de la fuente de alimentación.
![Montaje fuente, vista frontal](img/photo_2020-10-22_11-50-40.jpg "Vista frontal del montaje de la fuente de alimentación")


Una vez finalizado el montaje y realizadas las pruebas de voltaje con un multímetro esta podrá utilizarse de diversas formas.

En este caso, la fuente se utilizó para encender una tira de LEDS de 12[V].

![Fuente y tira de led](img/photo_2020-10-22_11-50-31.jpg "Fuente de alimentación y tira de delds de 12VCD")
