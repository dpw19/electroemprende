# Proyecto de emprendimiento

Fer ...
Laboratorio para arduino

# Descripcion del proyecto

Laboratorio de proyectos de Arduino

Cuenta con:
1. Fuente de voltaje DC y AC
1. Base para conectar módulo Arduino
1. Control de motores velocidad para motores
1. Control para motor a pasos unipolares
1. Control para motor a pasos bipolares
1. Instrumentos de medición
	1. Multímetro
	1. Osciloscopio 
1. Generador de señales
1. Dimer para manejar potencia en AC
1. Componentes Pasivos y activos
1. Tablilla de conexiones
1. Relevadores optoaislados

# Aportaciones al proyecto (Haziel)

## Qué tan viable es el proyecto

* Yo pienso que esta muy bien estructurado y claro.
* Perfecto para ponerlo en marcha

## Que dificultades le notas

* Siento que será un proyecto un poco largo y difícil
* Que es un proyecto demasiado grande y laborioso
 
## Qué sugieres para que sea más viable

* Con dedicación todo se puede :)


# Aportaciones al proyecto (Jacqueline)

## Qué tan viable es el proyecto

* Considero que el proyecto es viable y que puede tener diferentes utilidades y usos. 

## Que dificultades le notas

* Creo que se necesitaría definir el propósito del proyecto.
* creo que aún no me queda muy claras las funciones del proyecto. 
* El tiempo requerido para realizar el proyecto. 

## Qué sugieres para que sea más viable

* Realizar el proyecto de manera organizada (por pasos)
* Verificar que las funcionalidades se realicen correctamente. 


# Presentación del proyecto


# Pasos a seguir

