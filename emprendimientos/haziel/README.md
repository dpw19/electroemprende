# Proyecto de emprendimiento

Haziel ...

# Descripcion del proyecto

Consiste en colocar tres tiras de leds en los rines de la bicicleta prender los 
leds y con las revoluciónes de la llanta crear un efecto de que se ilumina la 
llanta esto ayudará a las bicicletas que van en la calle en la noches a que los
coches puedan verla

# Aportaciones al proyecto (Jacqeline)

## Qué tan viable es el proyecto

* Considero que es una buena idea.
* Que es viable ya que resulta ser una idea
* Atractiva para quien gusta de andar en bicicleta. 

## Que dificultades le notas
* Considero que lo más complicado sería la manera en que se instalarán las luces led
* Debe de ser un soporte que no se caiga con el movimiento. 

## Qué sugieres para que sea más viable

* Quizás se podría optar usar cinta doble o alguna otra idea para sostener las luces led.
* Se podría instalar en forma radial las tres tiras para que se vea más estético
* También se podrían usar diferentes colores para las luces led.

# Aportaciones al proyecto (Fer)

## Qué tan viable es el proyecto

## Que dificultades le notas

## Qué sugieres para que sea más viable


# Presentación del proyecto

![Diapositiva 01](imagen/photo_2020-07-23_17-37-47.jpg)

![Diapositiva 01](imagen/photo_2020-07-23_17-37-51.jpg)

![Diapositiva 01](imagen/photo_2020-07-23_17-37-54.jpg)

![Diapositiva 01](imagen/photo_2020-07-23_17-37-57.jpg)

![Diapositiva 01](imagen/photo_2020-07-23_17-37-59.jpg)


# Pasos a seguir

