# Proyecto a elaborar (Producto)

Lámpara de emergencia, recargable, con diseños artesanales.

Es una lámpara casera con Pantalla elaborada con lana con regulación de intensidad.

La construcción se basada en este [tutorial](https://www.handfie.com/tutorial/como-hacer-una-lampara-casera-con-lana/).

# Dibujo de cómo imaginas tu proyecto.


# Investigación inicial sobre el proyecto
Las lámparas son dispositivos que transforman una energía eléctrica o química en
energía lumínica.
Desde un punto de vista más técnico, se distingue entre dos objetos:
* **Lámpara:** Es el dispositivo que produce la luz.
* **Luminaria:** Es el soporte de la lámpara.

Informacin obtenida de esta [Wiki](https://es.wikipedia.org/wiki/L%C3%A1mpara)

## Iluminación LED a12 [V]

Existen muchas opciones para iluminar a 12V con corriente continua, la mayoría
de ellas provienen del mundo de la automoción. En nuestro caso buscaremos el
menor consumo energético para alargar la duración de la batería lo máximo
posible, por lo tanto recomendamos instalar iluminación LED 12V.

Si vamos a utilizar la iluminación al aire libre es importante que el LED y la
lámpara estén preparado para ello.

## Proteccion IP

Para exteriores se recomienda un gabinete IP65 protección fuerte contra polvo y
protección contra chorro de agua.

IP XX:  Nivel de protección para Sólidos y para Líquidos.

También se le conoce como NEMA

## Duración de la batería

En cuanto a la duración de la batería con el LED seleccionado, podemos hacer una
aproximación de la siguiente forma:

Si contamos con una batería de coche de 12 [V] a 100Ah, tendremos 1,200Wh
acumulados.

Con un led de 20 [W] este podrá lucir durante 60h.
Ya que $`1200[Wh] / 20 [W] = 60 [h]`$

Esta es la duración teórica dejando a cero la batería, cosa que no es posible
ya que, entre otras cosas, quedaría dañada si baja de 11,8V. Así que debemos
contar con menos autonomía que la teórica, un 50% de este número sería lo más
adecuado siendo conservadores: 30h.

Información obtenida de [nergiza](https://nergiza.com/iluminacion-portatil-a-12v-con-baterias-todo-lo-que-hay-que-saber/)

## Lamparas de emergencia

El alumbrado de emergencia está compuesto por dispositivos que funcionan a
través de una batería cuando la alimentación normal falla. Tiene varios
objetivos, entre los que destacan:
* Evitar la ausencia completa de luz en el interior
* Señalizar el acceso hasta las salidas normales o de emergencias en caso de que sea necesaria la evacuación del personal.

### Tipos de luces de emergencia

* **Alumbrado de seguridad:** Sirven para indicar las salidas de emergencia, y para
garantizar la seguridad de las personas que precisan ser evacuadas en caso de fallo eléctrico.

* **Alumbrado de reemplazamiento:** Su función reside en permitir la
continuidad de la actividad normal del espacio en caso de fallo eléctrico, sin ocasionar molestias para las personas que se encuentren en su interior.

* **Alumbrado ambiental:** Se trata de puntos de luz de ambiente, destinados a evitar situaciones de pánico en las personas ante la oscuridad total, y dando una visibilidad suficiente para localizar las vías de salida.

* **Alumbrado de zonas de trabajo con alto riesgo:** Se sitúan en lugares donde se realizan actividades o trabajos considerados de alto riesgo, de modo que garanticen la seguridad total de los trabajadores si falla la corriente general.

### Cómo funcionan

Una luz de emergencia está formada por una o varias lámparas que por lo general
se mantienen apagadas.

Cuentan con un pequeño punto de luz, un piloto de luz verde o roja
que se mantiene encendido cuando la lámpara está apagada, como garantía de su
correcto estado y funcionamiento.

Cada luz de emergencia cuenta con su propia batería, que no se activa si no se
da el caso de un corte de luz general.

De forma habitual, las lámparas se mantienen conectadas a la red eléctrica, que
carga la batería y mantiene el circuito activo para evitar fallos en su
funcionamiento.

En algunos modelos, se incluye un interruptor de prueba, que permite comprobar
que la lámpara se encuentra en buenas condiciones en cualquier momento puntual.

Además, es necesario realizar diversas pruebas de forma periódica para evitar el envejecimiento y el desgaste de los dispositivos.

### Modos de funcionamiento

Las luces de emergencia pueden presentar tres estados diferentes, dependiendo
del momento en que se encuentre:

1. La posición natural de las luces de emergencia, a la espera de un fallo
eléctrico para ponerse en funcionamiento.
1. Una luz de emergencia en estado completo de reposo, habrá sido apagada
deliberadamente por alguien, o habrá agotado la duración de su batería. Volverá
al estado de alerta en el momento en que reciba nuevamente corriente.

Informacion obtenida de [termiserprotecciones](http://termiserprotecciones.com/funcionamiento-luces-de-emergencia/#:~:text=As%C3%AD%2C%20existen%20varios%20tipos%20de,Alumbrado%20de%20reemplazamiento).


{+ Este proyecto puede diversificarse para ser también una lámparas de
  emergencia, lampara para campismo y para el hogar cuando falle el suministro eléctrico +}

## Baterías recargables

### Alcalinas
Las pilas alcalinas son las más comunes dentro de las pilas no recargables.

### Salinas

Las pilas salinas, o pilas de zinc-carbono, se encuentran cada vez mas en
desuso.

Tienen un coste menor que las alcalinas pero también menor capacidad. Puede que para algún uso sean convenientes, pero por lo general son mejores las pilas alcalinas.

### Litio

Existen varios tipos de pilas que incorporan litio en su composición.
Estos modelos se caracterizan por tener una autodescarga muy baja; si se
mantienen a 20 ºC se descargará un 1 % por año.

Las pilas rectangulares son menos comunes que las cilíndricas, pero aún hay aparatos que las utilizan.

Son de mayor tamaño y presentan diferentes voltajes, por encima de
los 4,5 voltios. Su voltaje nominal es de 3.7[V]

### Ácido Plomo 12 [V]

También denominada batería de ácido-plomo es un
tipo de batería muy común en vehículos convencionales, como batería de arranque,
aunque también se utilizan como batería de tracción de vehículos eléctricos.

Información obtenida de [wiki](https://es.wikipedia.org/wiki/Bater%C3%ADa_de_plomo_y_%C3%A1cido)
y de [actitudtecnologica](https://actitudecologica.com/tipos-de-pilas/).

Para el proyecto de la lámpara utilizaremos un arreglo pilas le Litio. Por ello
es conveniente conocer la forma de cargarlas y la forma de conectarlas entre sí
para obtener un voltaje adecuado al proyecto.

# Por investigar

* {+ Cómo elevar el voltaje de 3.7 [V] a 12[V]. +}
* {+ Fuente DC/DC tipo bost o tipo Buck +}

# Construcción de la pantalla de estambre

Instrucciones:

1. Inflar un globo.
2. Preparar engrudo.
3. Pegar el estambre con engrudo sobre el globo.
4. Dejar secar el engrudo hasta que hilo este rígido.
5. Cortar el globo por la mitad.
